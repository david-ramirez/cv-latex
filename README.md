# Curriculum Vitae

Mi CV hecho a Latex basado en la plantilla de 
[latextemplates.com](https://www.latextemplates.com/template/twenty-seconds-resumecv), 
basada a su vez en el trabajo de [Carmine Spagnuolo](https://github.com/spagnuolocarmine/TwentySecondsCurriculumVitae-LaTex).